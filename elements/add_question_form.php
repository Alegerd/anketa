<form id="addQuestionForm" name="addQuestionForm" method="post">
    <table width="309" border="0" align="center" cellpadding="2" cellspacing="5">
        <tr>
            <td colspan="2">
                <h4> New Question </h4>
            </td>
        </tr>
        <tr>
            <td width="116"><div align="right">Question</div></td>
            <td width="177"><input name="name" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">Answer</div></td>
            <td><input name="value" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right"></div></td>
            <td><input id="add-new-question-btn" class="waves-effect waves-light btn" name="" type="button" value="Go" /></td>
        </tr>
    </table>
</form>
