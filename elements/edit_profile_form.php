<form id="editProfile" name="editProfile" method="post" enctype="multipart/form-data">
    <table width="309" border="0" align="center" cellpadding="2" cellspacing="5">
        <tr>
            <td colspan="2">
                <h4> Edit Profile </h4>
            </td>
        </tr>
        <tr>
            <td width="116"><div align="right">First Name</div></td>
            <td width="177"><input name="first_name" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">Last Name</div></td>
            <td><input name="last_name" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">Profession</div></td>
            <td><input name="job" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">Address</div></td>
            <td><input name="address" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">E-mail</div></td>
            <td><input name="email" type="text" /></td>
        </tr>
        <tr>
            <td><div align="right">Phone</div></td>
            <td><input name="phone" type="text" /></td>
        </tr>
        <td  colspan="2">
            <div class = "file-field input-field">
                <div class = "btn">
                    <span>avatar</span>
                    <input type = "file" />
                </div>

                <div class = "file-path-wrapper">
                    <input class = "file-path validate" type = "text"
                           placeholder = "Upload file" />
                </div>
            </div>
        </td>
        <tr>
            <td><div align="right"></div></td>
            <td><input id="edit-profile-btn" class="waves-effect waves-light btn" name="" type="button" value="Edit" /></td>
        </tr>
    </table>
</form>
