<?php
session_start();

include('connection.php');
require_once ('utils.php');
require_once ('profile_service.php');


$name=$_POST['name'];
$description=$_POST['description'];
$userId=$_POST['userId'];

mysqli_query($bd, "INSERT INTO anketas(name, description, userId) VALUES ('$name', '$description', '$userId')");
$result=mysqli_query($bd, "SELECT * FROM anketas WHERE name='$name' and userId='$userId'");
$result = mysqli_fetch_assoc($result);
mysqli_close($bd);

echo '<div id="' . $result['name'] . '-anketa-panel">
                           <div class="w3-container w3-margin-top w3-card w3-white w3-padding-16">
                           <h2 class="w3-text-grey"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>' . $result['name'] . '</h2>
                           <h4 class="w3-text-grey  w3-padding-16">' . $result['description'] . '</h4>';

$questions = getQuestionsByAnketaId($result['id']);

echo '<div id="'.$result['id'].'-question-panel">';
if(is_array($questions)) {
    foreach ($questions as &$question) {
        echo '<div class="w3-container">
            <h5 class="w3-opacity"><b>'.$question['name'].'</b></h5>
            <p>'.$question['answer'].'</p>
            <hr></div>';
    }
}
echo "</div>";

echo '<a onclick="addQuestionFormBtnOnClickEvent(\''.$result['id'].'-add-question-form\')" class="waves-effect waves-light btn add-question-btn">add/change</a>
        <div id="'.$result['id'].'-add-question-form" class="invisible">
            <form id="'.$result['id'].'-question-form" name="\''.$result['id'].'-question-form" method="post">
                <table width="309" border="0" align="center" cellpadding="2" cellspacing="5">
                    <tr>
                        <td colspan="2">
                            <h4> New Question </h4>
                        </td>
                    </tr>
                    <tr>
                        <td width="116"><div align="right">Question</div></td>
                        <td width="177"><input name="name" type="text" /></td>
                    </tr>
                    <tr>
                        <td><div align="right">Answer</div></td>
                        <td><input name="answer" type="text" /></td>
                    </tr>
                    <tr>
                        <td><div align="right"></div></td>
                        <td><input onclick="addQuestionToBoardById(\''.$result['id'].'\', \''.$result['id'].'-question-form\', \''.$result['id'].'-question-panel\')" class="waves-effect waves-light btn" name="" type="button" value="Go" /></td>
                    </tr>
                </table>
            </form>
        </div><a onclick="deleteAnketa(\'' . $result['id'] . '\')" class="delete-link">remove board</a></div></div>';
?>