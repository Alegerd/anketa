<?php
require_once('utils.php');

function getUserById($userId){
    $bd = bd();
    $result = mysqli_query($bd, "SELECT * FROM users WHERE id='$userId'");
    mysqli_close($bd);
    $user = mysqli_fetch_assoc($result);
    return $user;
}

function getSkillsByUserId($userId) {
    $bd = bd();
    $result = mysqli_query($bd, "SELECT * FROM skills WHERE user_id='$userId'");
    mysqli_close($bd);
    while($row=mysqli_fetch_assoc($result)){
        $skills[] = $row;
    }
    return $skills;
}

function getLanguagesByUserId($userId) {
    $bd = bd();
    $result = mysqli_query($bd, "SELECT * FROM languages WHERE userId='$userId'");
    mysqli_close($bd);
    while($row=mysqli_fetch_assoc($result)){
        $languages[] = $row;
    }
    return $languages;
}

function getAnketasByUserId($userId) {
    $bd = bd();
    $result = mysqli_query($bd, "SELECT * FROM anketas WHERE userId='$userId'");
    mysqli_close($bd);
    while($row=mysqli_fetch_assoc($result)){
        $anketas[] = $row;
    }
    return $anketas;
}

function getQuestionsByAnketaId($anketaId) {
    $bd = bd();
    $result = mysqli_query($bd, "SELECT * FROM questions WHERE anketa_id='$anketaId'");
    mysqli_close($bd);
    while($row=mysqli_fetch_assoc($result)){
        $anketas[] = $row;
    }
    return $anketas;
}
?>