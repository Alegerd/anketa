<?php
function getUserUrl($userId){
    $data = array(
        'userId' => $userId,
    );

    $params = http_build_query($data, '', '&amp;');
    return $params;
}

function getParamFromUrl($url, $paramName) {
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    return $query[$paramName];
}

function bd(){
    $mysql_hostname = "localhost";
    $mysql_user = "root";
    $mysql_password = "root";
    $mysql_database = "anketa";

    $bd = mysqli_connect($mysql_hostname, $mysql_user, $mysql_password) or die("Could not connect database");
    mysqli_select_db($bd, $mysql_database) or die("Could not select database");
    return $bd;
}

function checkUserExists($username){
    $bd = bd();
    $checkUser = mysqli_query($bd, "SELECT * FROM users WHERE username='$username'");
    if(mysqli_num_rows($checkUser) > 0) {
        $_SESSION['SESS_ERROR_MSG'] = "User with such username already exists";
        mysqli_close($bd);
        return true;
    }
    mysqli_close($bd);
    return false;
}
?>
