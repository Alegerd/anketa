<?php
require_once('scripts/users_service.php');

    $users = getAllUsers();
?>
<!DOCTYPE html>
<html>
<title>Users</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/materialize/css/materialize.min.css">
<link rel="stylesheet" type="text/css" href="css/profile.css">
<link rel="stylesheet" type="text/css" href="css/users.css">
<style>
    html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>
<body class="w3-light-grey">

<div class="w3-content w3-margin-top" style="max-width:1400px;">
    <h1>Users</h1>
<?php
if(is_array($users)) {
foreach ($users as &$user) {
    echo '<div onclick="location.href=\'profile.php?userId='.$user['id'].'\';" class="user-card w3-row-padding w3-margin-top w3-card-4 padding0" >
            <div class="w3-quarter padding0">
                <div class="w3-display-container">
                    <img src="https://1001freedownloads.s3.amazonaws.com/vector/thumb/75167/1366695174.png" style="width:100%" alt="Avatar">
                </div>
            </div>
            
            <div class="w3-threequarter">
                <h5 class="w3-text-grey">'.$user['first_name'].' '.$user['last_name'].'</h5>
                <table border="0" align="center">
                    <tr>
                        <td>
                            <table border="0" align="center">
                                <tr>
                                    <td><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i></td>
                                    <td>
                                        '.$user['job'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i></td>
                                    <td>
                                        '.$user['address'].'
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                           <table border="0" align="center">
                                <tr>
                                    <td><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i></td>
                                    <td>
                                        '.$user['email'].'
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i></td>
                                    <td>
                                        '.$user['phone'].'
                                    </td>
                                </tr>
                            </table>    
                        </td>
                    </tr>
                </table>
            </div>
          </div>';
    }
}
?>
</div>
</body>
</html>