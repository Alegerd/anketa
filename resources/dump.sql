  CREATE TABLE `users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(255) NOT NULL,
	`last_name` VARCHAR(255) NOT NULL,
	`username` VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`photo` VARCHAR(255),
	`description` VARCHAR(255),
	`job` VARCHAR(255),
	`address` VARCHAR(255),
	`email` VARCHAR(255),
	`phone` VARCHAR(255),
	PRIMARY KEY (`id`)
);

CREATE TABLE `anketas` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`description` VARCHAR(255),
	`userId` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `questions` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`answer` VARCHAR(255) NOT NULL,
	`anketa_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `skills` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`value` INT NOT NULL,
	`user_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `languages` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`value` INT NOT NULL,
	`userId` INT NOT NULL,
	PRIMARY KEY (`id`)
);

ALTER TABLE `anketas` ADD CONSTRAINT `anketas_fk0` FOREIGN KEY (`userId`) REFERENCES `users`(`id`);

ALTER TABLE `questions` ADD CONSTRAINT `questions_fk0` FOREIGN KEY (`anketa_id`) REFERENCES `anketas`(`id`) ON DELETE CASCADE;

ALTER TABLE `skills` ADD CONSTRAINT `skills_fk0` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);

ALTER TABLE `languages` ADD CONSTRAINT `languages_fk0` FOREIGN KEY (`userId`) REFERENCES `users`(`id`);