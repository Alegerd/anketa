<?php
session_start();
unset($_SESSION['SESS_MEMBER_ID']);
unset($_SESSION['SESS_FIRST_NAME']);
unset($_SESSION['SESS_LAST_NAME']);
?>
<link rel="stylesheet" type="text/css" href="css/materialize/css/materialize.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

<div class="content">
    <form name="loginform" action="scripts/login_exec.php" method="post">
        <table width="309" border="0" align="center" cellpadding="2" cellspacing="5">
            <tr>
                <td colspan="2">
                    <h4> Login </h4>
                    <?php
                    if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
                        foreach($_SESSION['ERRMSG_ARR'] as $msg) {
                            echo '<div class="row">
                                    <div class="col s12 m12">
                                      <div class="card blue-grey darken-1">
                                        <div class="card-content white-text">
                                          <span class="card-title">Sorry</span>
                                          <p>'.$msg.'</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>';
                        }
                        unset($_SESSION['ERRMSG_ARR']);
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td width="116"><div align="right">Username</div></td>
                <td width="177"><input name="username" type="text" /></td>
            </tr>
            <tr>
                <td><div align="right">Password</div></td>
                <td><input name="password" type="password" /></td>
            </tr>
            <tr>
                <td><div align="right"></div></td>
                <td><input class="waves-effect waves-light btn" name="" type="submit" value="login" /></td>
            </tr>
        </table>
        <a class="w3-margin-left" href="registration.php" >Registration</a>
    </form>
</div>