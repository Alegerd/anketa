<?php
session_start();
unset($_SESSION['ADD_SKILL_SHOWN']);
unset($_SESSION['ADD_LANGUAGE_SHOWN']);
require_once('scripts/auth.php');
require_once ('scripts/profile_service.php');

$userId = $_GET['userId'];
$user = getUserById($userId);

?>
<!DOCTYPE html>
<html>
<title>Anketa</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/materialize/css/materialize.min.css">
<link rel="stylesheet" type="text/css" href="css/profile.css">
<style>
    html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>
<body class="w3-light-grey">

<div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

        <!-- Left Column -->
        <div class="w3-third">

            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-display-container">
                    <img src="https://1001freedownloads.s3.amazonaws.com/vector/thumb/75167/1366695174.png" style="width:100%" alt="Avatar">
                    <div class="w3-display-bottomleft w3-container w3-text-black">
                        <?php
                        echo '<h2>'.$user['first_name'].' '.$user['last_name'].'</h2>'
                        ?>
                    </div>
                </div>
                <div class="w3-container">
                    <?php
                        if($_SESSION['SESS_MEMBER_ID'] == $userId) {
                            echo '<a class="waves-effect waves-light btn edit-profile-btn w3-margin-top" > edit</a >
                                    <a class="w3-margin-left" href = "login.php" > Logout</a >
                                    <div id = "edit-profile-form" class="invisible" >';
                                            include('elements/edit_profile_form.php');
                                    echo '</div >';
                        }
                        ?>
                    <p><i class="fa fa-briefcase fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <?php
                            echo $user['job'];
                        ?>
                    </p>
                    <p><i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <?php
                            echo $user['address'];
                        ?>
                    </p>
                    <p><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <?php
                            echo $user['email'];
                        ?>
                    </p>
                    <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>
                        <?php
                        echo $user['phone'];
                        ?>
                    </p>
                    <hr>
                    <a class="w3-margin-left" href="users.php" >Show other users</a>
                    <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i>Skills</b></p>
                    <?php
                        if($_SESSION['SESS_MEMBER_ID'] == $userId) {
                            echo '<a class="waves-effect waves-light btn add-skill-btn" > add / change</a >
                                    <div id = "add-skill-form" class="invisible" >';
                                        include('elements/add_skill_form.php');
                                        echo '</div>';
                    }
                    ?>
                    <div id="skill-box">
                    <?php
                    $skills = getSkillsByUserId($userId);
                    if(is_array($skills)) {
                        foreach ($skills as &$skill) {
                            echo '<div id="' . $skill['name'] . '-skill-panel"><p>' . $skill['name'] . '</p>
                                <div class="w3-light-grey w3-round-xlarge w3-small">
                                    <div id="skill-' . $skill['name'] . '-card" class="widget-value w3-container w3-center w3-round-xlarge w3-teal" style="width:' . $skill['value'] . '%">' . $skill['value'] . '%</div>
                                </div>
                                <a onclick="deleteSkill(\'' . $skill['name'] . '\', \'' . $userId . '\')" class="delete-link">remove</a></div>';
                        }
                    }

                    unset($skill);
                    ?>
                    </div>
                    <br>

                    <p class="w3-large w3-text-theme"><b><i class="fa fa-globe fa-fw w3-margin-right w3-text-teal"></i>Languages</b></p>
                    <?php
                        if($_SESSION['SESS_MEMBER_ID'] == $userId) {
                        echo  '<a class="waves-effect waves-light btn add-language-btn">add/change</a>
                                <div id="add-language-form" class="invisible">';

                        include('elements/add_language_form.php');
                        echo '</div>';
                        }
                    ?>
                    <div id="language-box">
                        <?php
                        $languages = getLanguagesByUserId($userId);
                        if(is_array($languages)) {
                            foreach ($languages as &$language) {
                                echo '<div id="' . $language['name'] . '-language-panel"><p>' . $language['name'] . '</p>
                                <div class="w3-light-grey w3-round-xlarge w3-small">
                                    <div id="language-' . $language['name'] . '-card" class="widget-value w3-container w3-center w3-round-xlarge w3-teal" style="width:' . $language['value'] . '%">' . $language['value'] . '%</div>
                                </div>
                                <a onclick="deleteLanguage(\'' . $language['name'] . '\', \'' . $userId . '\')" class="delete-link">remove</a></div>';
                            }
                        }

                        unset($language);
                        ?>
                    </div>
                    <br>
                </div>
            </div><br>

            <!-- End Left Column -->
        </div>

        <!-- Right Column -->
        <div class="w3-twothird">
            <div id="anketa-box">
            <?php
            $anketas = getAnketasByUserId($userId);
            if(is_array($anketas)) {
                foreach ($anketas as &$anketa) {
                    echo '<div id="' . $anketa['id'] . '-anketa-panel">
                           <div class="w3-container w3-margin-top w3-card w3-white w3-padding-16">
                           <h2 class="w3-text-grey"><i class="fa fa-certificate fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>' . $anketa['name'] . '</h2>
                           <h4 class="w3-text-grey  w3-padding-16">' . $anketa['description'] . '</h4>';

                            $questions = getQuestionsByAnketaId($anketa['id']);

                            echo '<div id="'.$anketa['id'].'-question-panel">';
                            if(is_array($questions)) {
                                foreach ($questions as &$question) {
                                    echo '<div class="w3-container">
                                            <h5 class="w3-opacity"><b>'.$question['name'].'</b></h5>
                                            <p>'.$question['answer'].'</p>
                                            <hr></div>';
                                }
                            }
                            echo "</div>";

                    echo '<a onclick="addQuestionFormBtnOnClickEvent(\''.$anketa['id'].'-add-question-form\')" class="waves-effect waves-light btn add-question-btn">add/change</a>
                <div id="'.$anketa['id'].'-add-question-form" class="invisible">
                    <form id="'.$anketa['id'].'-question-form" name="\''.$anketa['id'].'-question-form" method="post">
                        <table width="309" border="0" align="center" cellpadding="2" cellspacing="5">
                            <tr>
                                <td colspan="2">
                                    <h4> New Question </h4>
                                </td>
                            </tr>
                            <tr>
                                <td width="116"><div align="right">Question</div></td>
                                <td width="177"><input name="name" type="text" /></td>
                            </tr>
                            <tr>
                                <td><div align="right">Answer</div></td>
                                <td><input name="answer" type="text" /></td>
                            </tr>
                            <tr>
                                <td><div align="right"></div></td>
                                <td><input onclick="addQuestionToBoardById(\''.$anketa['id'].'\', \''.$anketa['id'].'-question-form\', \''.$anketa['id'].'-question-panel\')" class="waves-effect waves-light btn" name="" type="button" value="Add" /></td>
                            </tr>
                        </table>
                    </form>
                </div><a onclick="deleteBoard(\'' . $anketa['id'] . '\')" class="delete-link">remove board</a></div></div>';
                }
            }

            unset($language);
            ?>

            </div>
            <?php
                if($_SESSION['SESS_MEMBER_ID'] == $userId) {
            echo '<div class="w3-container w3-card w3-white w3-margin-top">';
                include('elements/add_anketa_form.php');
            echo '</div>';
            }
            ?>

            <!-- End Right Column -->
        </div>

        <!-- End Grid -->
    </div>

    <!-- End Page Container -->
</div>

<footer class="w3-container w3-teal w3-center w3-margin-top">
    <p>Find me on social media.</p>
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
</footer>

</body>
</html>
