function deleteSkill(skillName, userId) {
    $.ajax({
        url: "scripts/remove_skill_exec.php",
        method: "POST",
        data: { 'name': skillName, 'userId' : userId},
        success: function () {
            $('#' + skillName + '-skill-panel').remove();
        }
    })
};

function deleteLanguage(languageName, userId) {
    $.ajax({
        url: "scripts/remove_language_exec.php",
        method: "POST",
        data: { 'name': languageName, 'userId' : userId},
        success: function () {
            $('#' + languageName + '-language-panel').remove();
        }
    })
};

function deleteBoard(boardId) {
    $.ajax({
        url: "scripts/remove_board_exec.php",
        method: "POST",
        data: { 'id': boardId},
        success: function () {
            $('#' + boardId + '-anketa-panel').remove();
        }
    })
}

function addQuestionFormBtnOnClickEvent(questionPanelId) {
    if(localStorage.getItem(questionPanelId + "-visible") == null || localStorage[questionPanelId + '-visible'] == "no") {
        $('#' + questionPanelId).removeClass("invisible");
        localStorage[questionPanelId + '-visible'] = "yes";
    } else {
        $('#' + questionPanelId).addClass("invisible");
        localStorage[questionPanelId + '-visible'] = "no";
    }
}

function addQuestionToBoardById(anketaId, questionFormId, questionPanelId) {
    var formData = $('#' + questionFormId).serialize();
    formData += '&anketaId=' + anketaId;
    $.ajax({
        url: "scripts/add_question_exec.php",
        method: "POST",
        data: formData,
        success: function (data) {
            $('#' + questionPanelId).after(data);
        }
    })
}

$(document).ready(function(){

    $('.add-skill-btn').click(function () {
        if(localStorage.getItem("skill-visible") == null || localStorage['skill-visible'] == "no") {
            $('#add-skill-form').removeClass("invisible");
            localStorage['skill-visible'] = "yes";
        } else {
            $('#add-skill-form').addClass("invisible");
            localStorage['skill-visible'] = "no";
        }
    });

    $('.add-language-btn').click(function () {
        if(localStorage.getItem("lang-visible") == null || localStorage['lang-visible'] == "no") {
            $('#add-language-form').removeClass("invisible");
            localStorage['lang-visible'] = "yes";
        } else {
            $('#add-language-form').addClass("invisible");
            localStorage['lang-visible'] = "no";
        }
    });

    $('.edit-profile-btn').click(function () {
        if(localStorage.getItem("edit-visible") == null || localStorage['edit-visible'] == "no") {
            $('#edit-profile-form').removeClass("invisible");
            localStorage['edit-visible'] = "yes";
        } else {
            $('#edit-profile-form').addClass("invisible");
            localStorage['edit-visible'] = "no";
        }
    });

    var urlParams = new URLSearchParams(window.location.search);
    var userId = urlParams.get('userId');

    $('#edit-profile-btn').click(function () {
        var formData = $('#editProfile').serialize();
        formData += '&userId=' + userId;
        $.ajax({
            url: "scripts/edit_profile_exec.php",
            method: "POST",
            data: formData,
            success: function (data) {
                document.location.reload(true);
            }
        })
    });

    $('#add-new-skill-btn').click(function () {
        var formData = $('#addSkillForm').serialize();
        formData += '&userId=' + userId;
        $.ajax({
            url: "scripts/add_skill_exec.php",
            method: "POST",
            data: formData,
            success: function (data) {
                var element = new DOMParser().parseFromString(data, "text/xml").firstChild;
                if(element.classList.contains("widget-value")) {
                    $('#' + element.id).replaceWith(data)
                } else {
                    $('#skill-box').after(data);
                }
            }
        })
    });

    $('#add-new-language-btn').click(function () {
        var formData = $('#addLanguageForm').serialize();
        formData += '&userId=' + userId;
        $.ajax({
            url: "scripts/add_language_exec.php",
            method: "POST",
            data: formData,
            success: function (data) {
                var element = new DOMParser().parseFromString(data, "text/xml").firstChild;
                if(element.classList.contains("widget-value")) {
                    $('#' + element.id).replaceWith(data)
                } else {
                    $('#language-box').after(data);
                }
            }
        })
    })

    $('#add-new-anketa-btn').click(function () {
        var formData = $('#addAnketaForm').serialize();
        formData += '&userId=' + userId;
        $.ajax({
            url: "scripts/add_anketa_exec.php",
            method: "POST",
            data: formData,
            success: function (data) {
                $('#anketa-box').after(data);
            }
        })
    })
});