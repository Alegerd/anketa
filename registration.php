<?php
session_start();
unset($_SESSION['SESS_MEMBER_ID']);
require_once('scripts/utils.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" type="text/css" href="css/materialize/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script type="text/javascript">
        function validateForm()
        {
            var a=document.forms["reg"]["fname"].value;
            var b=document.forms["reg"]["lname"].value;
            var f=document.forms["reg"]["username"].value;
            var g=document.forms["reg"]["password"].value;
            if (a==null || a=="")
            {
                alert("Введите имя");
                return false;
            }
            if (b==null || b=="")
            {
                alert("Введите фамилию");
                return false;
            }
            if (f==null || f=="")
            {
                alert("Введите ");
                return false;
            }
            if (g==null || g=="")
            {
                alert("password must be filled out");
                return false;
            }
        }
        </script>
</head>
<body>

<div class="content">
    <form name="reg" action="scripts/registration_exec.php" onsubmit="return validateForm()" method="post">
        <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <h4> Registration </h4>
                    <?php
                    if(isset($_SESSION['SESS_ERROR_MSG'])){
                        echo '<div class="row">
                                    <div class="col s12 m12">
                                      <div class="card blue-grey darken-1">
                                        <div class="card-content white-text">
                                          <span class="card-title">Sorry</span>
                                          <p>'.$_SESSION['SESS_ERROR_MSG'].'</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>';
                    }
                    unset($_SESSION['SESS_ERROR_MSG'])
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="95"><div align="right">Имя:</div></td>
                            <td width="171"><input type="text" name="fname" /></td>
                        </tr>
                        <tr>
                            <td><div align="right">Фамилия:</div></td>
                            <td><input type="text" name="lname" /></td>
                        </tr>
                        <td><div align="right">Username:</div></td>
                        <td><input type="text" name="username" /></td>
                        </tr>
                        <tr>
                            <td><div align="right">Password:</div></td>
                            <td><input type="password" name="password" /></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td><div align="right">Profession:</div></td>
                            <td><input type="text" name="job" /></td>
                        </tr>
                        <tr>
                            <td><div align="right">Address:</div></td>
                            <td><input type="text" name="address" /></td>
                        </tr>
                        <tr>
                            <td><div align="right">E-mail:</div></td>
                            <td><input type="text" name="email" /></td>
                        </tr>
                        <tr>
                            <td><div align="right">Phone:</div></td>
                            <td><input type="text" name="phone" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input class="waves-effect waves-light btn" name="submit" type="submit" value="Submit" />
                </td>
            </tr>
        </table>
        <a class="w3-margin-left" href="login.php" >Login</a>
    </form>
</div>
</body>
</html>